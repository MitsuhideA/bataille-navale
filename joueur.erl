-module(joueur).

-export([logon/1, joueur/2]).

%% L'emplacement du serveur (à changer sur une nouvelle machine)
server_node() ->
        serveur@RpiZak.

%% Fonction permettant d'envoyer une demande de connexion
logon(Name) ->
	%% Si le processus est déjà existant, on ne le recrée pas
	case whereis(processus_joueur) of 
		undefined ->
			register(processus_joueur, 
				spawn(joueur, joueur, [server_node(), Name]));
		_ -> already_logged_on
	end.

%% Fonction permettant d'envoyer un message au serveur avec les infos pour se connecter
joueur(Server_Node, Name) ->
	{serveur, Server_Node} ! {self(), logon, Name},
	await_result_login(),
	joueur(Server_Node).

%% Boucle principale qui attends les messages du serveur
joueur(Server_Node) ->
	receive
		%% Si un tir est demandé par le serveur
		ordreTir ->
			%% On demande à l'utilisateur de choisir une case et on envoie
			{serveur, Server_Node} ! demanderCase(),
			await_result_hit();
		{resultOtherPlayer, Case, Resultat} ->
			io:fwrite("Le joueur adverse a joué la case: ~w et il a ~w!~n", [Case, Resultat]); 
		{resultOtherPlayer, Case, Resultat, Bateau} ->
			io:fwrite("Le joueur adverse a joué la case: ~w et il a ~w ~w!~n", [Case, Resultat, Bateau]); 
		{victoire, NomJoueur} ->
			io:fwrite("Le joueur ~p gagne!", [NomJoueur]),
			exit(normal)
	end,
	%% On se replace en attente de messages
	joueur(Server_Node).
	
demanderCase() ->
	io:format("Veuillez entrer une case suivant cet exemple: (ex:Case>a2)~n", []),
	%% On récupère les infos
	case io:fread("Case>", "~1s~d") of
		{ok, [Lettre, Numero]} -> 	
			if
			Lettre >= "a" andalso Lettre =< "j" andalso Numero >= 0 andalso Numero =< 9 ->
				{self(), Lettre, Numero};
			true ->
				io:fwrite("Mauvaise entrée. Veuillez recommencer.~n", []),
				demanderCase()
			end;
		_Else -> demanderCase()
	end.

await_result_hit() ->
	receive
		{result, Resultat} ->
			io:format("Resultat:~p~n", [Resultat]);
		{result, Resultat, Boat} ->
			io:format("Resultat:~p ~p~n", [Resultat, Boat])
	end.

%% Fonction permettant d'attendre la réponse pour la connexion
await_result_login() ->
	receive
		% Si un problème intervient, on affiche pourquoi
		{serveur, stop, Why} -> 
			io:format("~p~n", [Why]),
			exit(normal);
		% Sinon on affiche que tout s'est bien passé
		{serveur, What} ->  
			io:format("~p~n", [What])
	end.

