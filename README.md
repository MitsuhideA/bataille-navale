# Bienvenue sur le répertoire de la Bataille Navale de Benjamin SUCHEL et Zakaria YAHI en Erlang ! #
-------------------------------------------------------------
## Configuration du jeu ##
Il faut modifier le fichier joueur.erl afin de mettre le nom de machine du serveur.
La fonction à modifier est ligne 7.

La position des bateaux est codée en dur, il faut donc aussi modifier le fichier.
Les bateaux se trouvent tout en bas.

## Lancer le jeu (sous Linux) ##
### Partie Serveur ###
* Ouvrir un terminal
* Lancer la commande : erl -sname serveur -setcookie test
* Entrer la commande : c(serveur).
* Puis la commande : serveur:start_server().

### Partie Joueur ###
* Ouvrir un terminal
* Lancer la commande : erl -sname nom_joueur -setcookie test
* Entrer la commande : c(joueur).
* Puis si le serveur est bien lancé : joueur:logon(votre_nom). (ex: joueur:logon(zak). )
* Le serveur se met en attente de deux joueurs, il faut alors répéter les manipulations pour le deuxième joueur.
