-module(serveur).

-export([start_server/0, serveur/1]).

%% Fonction qui reçoit les messages de connexions
serveur(ListeJoueurs) ->
	receive
		{From, logon, Name} ->
			NewListeJoueurs = serveur_logon(From, Name, ListeJoueurs),
			if
				%% Si 2 joueurs sont co, on lance la partie
				length(NewListeJoueurs) == 2 -> init_game(NewListeJoueurs);
				%% Sinon on continue à attendre les joueurs
				true -> serveur(NewListeJoueurs)
			end
	end.

%% Fonction appelée quand un message de connexion est reçu
%% Permet d'ajouter un utilisateur à la liste
%% Renvoie une liste de joueurs connectés
serveur_logon(From, Name, User_List) ->
	case lists:keymember(Name, 2, User_List) of
		true ->
			From ! {serveur, stop, user_exists_at_other_node},
			User_List;
		false ->
			io:fwrite("Pid connecté : ~p, Nom : ~p~n", [From, Name]),
			From ! {serveur, logged_on},
			[{From, Name} | User_List]
	end.

%% Permet de lancer le serveur en creant un process qui appelle la fonction serveur avec une liste vide
start_server() ->
	register(serveur, spawn(serveur, serveur, [[]])).

%% Initialise la partie
init_game(ListeJoueurs) ->
	%% Récupère les maps des joueurs
	MapJ1 = getBoatsJ1(),
	MapJ2 = getBoatsJ2(),
	%% Et lance la boucle de tirs des joueurs
	game(ListeJoueurs, MapJ1, MapJ2).

%% La boucle principale de jeu
game(NewListeJoueurs, MapJ1, MapJ2) ->
	%% Récupère le nom des joueurs et leurs PID
	Joueur1 = lists:nth(1, NewListeJoueurs),
	{PidJoueur1, NomJoueur1} = Joueur1,

	Joueur2 = lists:nth(2, NewListeJoueurs),
	{PidJoueur2, NomJoueur2} = Joueur2,

	%% Donne l'ordre de tir au joueur et récupère la nouvelle Map
	io:fwrite("Demande tir => ~w : ~w~n", [PidJoueur1, NomJoueur1]),
	NewMapJ2 = ordre_tir_joueur(PidJoueur1, PidJoueur2, MapJ2),
	io:fwrite("MapJ2 : ~w~n", [NewMapJ2]),
	%% Si la map est vide après le tir, le joueur gagne
	case isMapEmpty(NewMapJ2) of
		true 	-> victoire(NewListeJoueurs, NomJoueur1);
		false 	-> continue
	end,
	
	%% Donne l'ordre de tir au joueur et récupère la nouvelle Map
	io:fwrite("Demande tir => ~w : ~w~n", [PidJoueur2, NomJoueur2]),
	NewMapJ1 = ordre_tir_joueur(PidJoueur2, PidJoueur1, MapJ1),
	io:fwrite("MapJ1 : ~w~n", [NewMapJ1]),
	%% Si la map est vide après le tir, le joueur gagne sinon on continue le jeu
	case isMapEmpty(NewMapJ1) of
		true 	-> victoire(NewListeJoueurs, NomJoueur2);
		false	-> game(NewListeJoueurs, NewMapJ1, NewMapJ2)
	end.	

%%Fonction Victoire
victoire(ListeJoueurs, NomJoueur) ->
	Joueur1 = lists:nth(1, ListeJoueurs),
	{PidJoueur1, _NomJoueur1} = Joueur1,

	Joueur2 = lists:nth(2, ListeJoueurs),
	{PidJoueur2, _NomJoueur2} = Joueur2,
	
	PidJoueur1 ! {victoire, NomJoueur},
	PidJoueur2 ! {victoire, NomJoueur},
	io:fwrite("Joueur ~w gagne! ~n", [NomJoueur]),
	exit(normal).
	
%%Fonction qui permet de savoir si le bateau est vide
isMapEmpty(MapJ) ->
	SizeMap = maps:size(MapJ),
	if
		SizeMap == 0 -> true;
		SizeMap > 0  -> false
	end.
	
	
%% Fonction qui ordonne à un joueur de tirer
ordre_tir_joueur(PidJoueur, PidAutreJoueur, MapAutreJ) ->
	%% Envoie un message au joueur concerné
	PidJoueur ! ordreTir,
	receive
		%% Reçoit la lettre et le numéro de la case
		{_Joueur, Lettre, Numero} ->
			NewMapJ = isBoatHit(Lettre, Numero, MapAutreJ, PidJoueur, PidAutreJoueur),
			NewMapJ
	end.
	
%%Fonction qui supprime une case d'un bateau s'il est touché.
isBoatHit(Lettre, Numero, MapAutreJ, PidJoueur, PidAutreJoueur) ->
	Case = list_to_atom(Lettre ++ integer_to_list(Numero)),
	case find(Case, MapAutreJ) of
		true -> 
			Boat = getBoatMap(Case, MapAutreJ),
			NewMapAutreJ = maps:remove(Case, MapAutreJ),
			case mapContainBoat(maps:values(NewMapAutreJ), Boat) of
				false ->
					PidJoueur ! {result, coulé, Boat},
					PidAutreJoueur ! {resultOtherPlayer, Case, coulé, Boat},
					NewMapAutreJ;
				true ->
					PidJoueur ! {result, touché},
					PidAutreJoueur ! {resultOtherPlayer, Case, touché},
					NewMapAutreJ
			end;
		false -> 
			PidJoueur ! {result, raté},
			PidAutreJoueur ! {resultOtherPlayer, Case, raté},
			MapAutreJ
	end.
	
%%Fonction qui récupère le nom du bateau
getBoatMap(Case, MapJ)->
	Found = maps:find(Case, MapJ),
	element(2, Found).

mapContainBoat([],_) -> false;
mapContainBoat([H|T], X) ->
	if
		H == X -> true;
		true -> mapContainBoat(T,X)
	end.
	
%%Fonction qui cherche si la case est présente dans le bateau.
find(Key, HashMap)->
	%% Cherche la clé
	Found = maps:find(Key, HashMap),

	%% Si trouvée on retourne vrai sinon faux
	case Found of
			{ok, _} -> true;
			error -> false
	end.

	
getBoatsJ1()->
	#{
		a0 => aircraft, a1 => aircraft, a2 => aircraft, a3 => aircraft, a4 => aircraft, 
		d3 => battleship, e3 => battleship, f3 => battleship, g3 => battleship,
		i5 => submarine, i6 => submarine, i7 => submarine,
		d7 => cruiser, e7 => cruiser, f7 => cruiser,
		b8 => destroyer, b9 => destroyer	
	}.

getBoatsJ2()->
	#{
		b9 => aircraft, c9 => aircraft, d9 => aircraft, e9 => aircraft, f9 => aircraft, 
		i1 => battleship, i2 => battleship, i3 => battleship, i4 => battleship,
		b0 => submarine, c0 => submarine, d0 => submarine,
		d6 => cruiser, e6 => cruiser, f6 => cruiser,
		a8 => destroyer, a9 => destroyer	
	}.
